data "aws_caller_identity" "current" {}

locals {
  ami_owners = {
    ubuntu = "099720109477"
    alpine = "538276064493"
  }

  ami_name_search = {
    ubuntu = "*ubuntu-noble-24.04-*"
    alpine = "alpine-3.20*"
  }

  ami_flavor_search = {
    ubuntu = "Canonical, Ubuntu, 24.04"
    alpine = "Alpine Linux"
  }

  ami_size = {
    ubuntu = 8
    alpine = 1
  }
}

data "aws_ami" "ami" {
  for_each = local.need_defaut_ami_id ? { 0 = "enabled" } : {}

  most_recent        = true
  include_deprecated = false
  owners             = [local.ami_owners[var.ami_os]]

  filter {
    name   = "name"
    values = [local.ami_name_search[var.ami_os]]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "description"
    values = ["${local.ami_flavor_search[var.ami_os]}*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

data "aws_vpc" "default" {
  for_each = local.need_default_vpc ? { 0 = "enabled" } : {}

  default = true
}

data "aws_subnets" "this" {
  for_each = "" == var.subnet_id && local.need_default_vpc ? { 0 = "enabled" } : {}

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default[0].id]
  }
}

data "aws_instance" "this" {
  for_each = var.destroy == false ? { 0 = "enabled" } : {}

  filter {
    name   = "tag:aws:ec2spot:fleet-request-id"
    values = [aws_spot_fleet_request.this[0].id]
  }
}

data "template_file" "user_data" {
  for_each = { 0 = "enabled" }

  template = file("${path.module}/templates/${var.ami_os}_user_data.tmpl")
  vars = {
    allow_ssh                     = var.allow_ssh
    region                        = var.region
    base64_wg_server_config       = var.base64_vpn_server_config
    known_host_ssm_parameter_name = try(aws_ssm_parameter.this_known_hosts[0].name, "")
  }
}

data "aws_ssm_parameter" "this_known_hosts" {
  count = var.allow_ssh ? 1 : 0

  name = aws_ssm_parameter.this_known_hosts[0].name
}
