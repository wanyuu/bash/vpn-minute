####
# Credentials
####

variable "access_key" {
  description = "Credentials: AWS access key."
  type        = string
  default     = null
}

variable "secret_key" {
  description = "Credentials: AWS secret key. Pass this as a variable, never write password in the code."
  type        = string
  default     = null
}

variable "shared_credentials_file" {
  description = "Credentials: shared credential file."
  type        = string
  default     = null
}

variable "uuid" {
  description = "Unique ID for the vpn-minute resources"
  type        = string
  default     = ""
}

variable "region" {
  description = "AWS region where resources will be created"
  type        = string
}

####
# Configuration
####

variable "destroy" {
  description = "This forces a destroy."
  type        = bool
  default     = false
}

variable "vpc_id" {
  description = "ID of the VPC (leave empty to use default VPC)"
  type        = string
  default     = ""
}

variable "ami_os" {
  description = "What is the underlying OS to use."
  type        = string
  default     = "ubuntu"
}

variable "ami_id" {
  description = "ID of the Amazon Image to use (leave empty to use the latest Ubuntu)"
  type        = string
  default     = ""
}

variable "base64_vpn_server_config" {
  description = "VPN server configuration encoded in base64"
  type        = string
}

variable "application_name" {
  description = "Name of the application"
  type        = string
  default     = ""
}

variable "subnet_id" {
  description = "ID of the subnet use to deploy wireguard instance (leave empty to use default subnet)"
  type        = string
  default     = ""
}

variable "instance_type" {
  description = "Type of instance that will run Wireguard"
  type        = string
  default     = "t3.nano"
}

variable "public_key" {
  description = "Public ssh key"
  type        = string
  default     = ""
}

variable "allow_ssh" {
  description = "Allow inbound ssh"
  type        = bool
  default     = false
}
